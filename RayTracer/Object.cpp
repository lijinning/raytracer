#include <cstdio>
#include <iostream>
#include <cstring>
#include <cmath>
#include <string>
#include <cstdlib>
#include <stdio.h>
#include <algorithm>
#include "Object.h"
#include <vector>
#include "Ray.h"

using namespace std;

namespace Tracer
{

	bool zero(float f) {
		return abs(f) < 10e-6;
	}

	int& Triangle::operator[] (int i)
	{
		assert(i >= 0 && i<3);
		return _p[i];
	}
	const int& Triangle::operator[] (int i) const
	{
		assert(i >= 0 && i<3);
		return _p[i];
	}
	Vec3f Triangle::operator() (int i)
	{
		assert(i >= 0 && i<3);
		return obj->m_pVertexList[_p[i]];
	}
	const Vec3f Triangle::operator() (int i) const
	{
		assert(i >= 0 && i<3);
		return obj->m_pVertexList[_p[i]];
	}
	Vec3f Triangle::GetNormal() // undefine direction
	{
		return ((obj->m_pVertexList[_p[1]] - obj->m_pVertexList[_p[0]]).Cross
		(obj->m_pVertexList[_p[2]] - obj->m_pVertexList[_p[0]])).Normalize();
	}
    Object::Object(void)
    {
        m_nVertices = -1;
        m_nTriangles = -1;
        m_pTriangleList = NULL;
        m_pVertexList = NULL;
		MaxPoint._p[0] = MaxPoint._p[1] = MaxPoint._p[2] = -2e10;
		MinPoint._p[0] = MinPoint._p[1] = MinPoint._p[2] = 2e10;
    }

    Object::~Object(void)
    {
        Destroy();
    }

    void Object::Destroy()
    {
        if(m_pTriangleList)
            delete []m_pTriangleList;
        if(m_pVertexList)
            delete []m_pVertexList;

        m_nVertices = -1;
        m_nTriangles = -1;
        m_pVertexList = NULL;
        m_pTriangleList = NULL;
    }

    bool Object::LoadFromObj(const char* fn)
    {
        FILE* fp = fopen(fn,"r");
        if(fp==NULL)
        {
            printf("Error: Loading %s failed.\n",fn);
            return false;
        }
        else
        {
            if(Parse(fp))
            {
                printf("Loading from %s successfully.\n",fn);
                printf("Vertex Number = %d\n",m_nVertices);
                printf("Triangle Number = %d\n",m_nTriangles);
				printf("MaxPoint = (%f, %f, %f)\n", MaxPoint.x, MaxPoint.y, MaxPoint.z);
				printf("MinPoint = (%f, %f, %f)\n", MinPoint.x, MinPoint.y, MinPoint.z);
                fclose(fp);
                return true;
            }
            else
            {
                fclose(fp);
                return false;
            }
        }
    }

    bool Object::Parse(FILE* fp)
    {
        
        char buf[256];
        int nVertices,nTriangles;
        std::vector<Vec3f>          vecVertices;
        std::vector<Triangle>  vecTriangles;

        nVertices = 0;
        nTriangles = 0;
        vecVertices.clear();
        vecTriangles.clear();
        int lineNumber = 0; 

        while(fscanf(fp, "%s", buf) != EOF)
        {
            lineNumber ++;
            switch(buf[0])
            {
            case '#':				/* comment */
                /* eat up rest of line */
                fgets(buf, sizeof(buf), fp);
                break;
            case 'v':				/* v, vn, vt */
                switch(buf[1])
                {
                case '\0':			    /* vertex */
                    {
                        Vec3f vP;
                        if(fscanf(fp, "%f %f %f", 
                            &vP.x, 
                            &vP.y, 
                            &vP.z)==3)
                        {
                            nVertices++;
                            vecVertices.push_back(vP);
							MaxPoint.x = vP.x > MaxPoint.x ? vP.x : MaxPoint.x;
							MaxPoint.y = vP.y > MaxPoint.y ? vP.y : MaxPoint.y;
							MaxPoint.z = vP.z > MaxPoint.z ? vP.z : MaxPoint.z;
							MinPoint.x = vP.x < MinPoint.x ? vP.x : MinPoint.x;
							MinPoint.y = vP.y < MinPoint.y ? vP.y : MinPoint.y;
							MinPoint.z = vP.z < MinPoint.z ? vP.z : MinPoint.z;
                        }
                        else 
                        {
                            printf("Error: Wrong Number of Values(Should be 3). at Line %d\n",lineNumber);
                            return false;
                        }
                    }
                    break;
                default:
                    /* eat up rest of line */
	                fgets(buf, sizeof(buf), fp);
                    break;
                }
                break;

            case 'f':				/* face */
                {
                    int v,n,t;
                    Triangle vIndices;
                    if(fscanf(fp, "%s", buf)!=1)
                    {
                        printf("Error: Wrong Face at Line %d\n",lineNumber);
                        return false;
                    }
                    
                    /* can be one of %d, %d//%d, %d/%d, %d/%d/%d %d//%d */
                    if (strstr(buf, "//"))
                    {
                        /* v//n */
                        if( sscanf(buf, "%d//%d", &vIndices[0],&n) ==2  &&
                            fscanf(fp, "%d//%d", &vIndices[1], &n) ==2  &&
                            fscanf(fp, "%d//%d", &vIndices[2], &n) ==2)
                        {
                            nTriangles++;
                            vecTriangles.push_back(vIndices);
                        }
                        else
                        {
                            printf("Error: Wrong Face at Line %d\n",lineNumber);
                            return false;
                        }

                    }
                    else if (sscanf(buf, "%d/%d/%d", &v, &t, &n) == 3)
                    {
                        /* v/t/n */
                        vIndices[0] = v;
                        if( fscanf(fp, "%d/%d/%d", &vIndices[1], &t, &n) ==3 &&
                            fscanf(fp, "%d/%d/%d", &vIndices[2], &t, &n) ==3 )
                        {
                            nTriangles++;
                            vecTriangles.push_back(vIndices);
                        }
                        else
                        {
                            printf("Error: Wrong Face at Line %d\n",lineNumber);
                            return false;
                        }
                    }
                    else if (sscanf(buf, "%d/%d", &v, &t) == 2)
                    {
                        /* v/t */
                        vIndices[0] = v;
                        if( fscanf(fp, "%d/%d", &vIndices[1], &t) ==2 &&
                            fscanf(fp, "%d/%d", &vIndices[2], &t) ==2 )
                        {
                            nTriangles++;
                            vecTriangles.push_back(vIndices);
                        }
                        else
                        {
                            printf("Error: Wrong Face at Line %d\n",lineNumber);
                            return false;
                        }
                    }
                    else
                    {
                        /* v */
                        if( sscanf(buf, "%d", &vIndices[0]) ==1 && 
                            fscanf(fp, "%d", &vIndices[1])  ==1 &&
                            fscanf(fp, "%d", &vIndices[2])  ==1 )
                        {
                            nTriangles++;
                            vecTriangles.push_back(vIndices);
                        }
                        else
                        {
                            printf("Error: Wrong Face at Line %d\n",lineNumber);
                            return false;
                        }
                    }

                }

                break;

            default:
                /* eat up rest of line */
                fgets(buf, sizeof(buf), fp);
                break;
            }
        }
        
        if(CheckParse(vecVertices.size(),vecTriangles))
        {
            Destroy();
            
            m_nVertices = vecVertices.size();
            m_nTriangles = vecTriangles.size();            
            m_pVertexList = new Vec3f[m_nVertices];
            m_pTriangleList = new Triangle [m_nTriangles];

            for(int i=0;i<m_nVertices;i++)
                m_pVertexList[i] = vecVertices[i];
            for(int i=0;i<m_nTriangles;i++)
            {
                for(int j=0;j<3;j++)
                m_pTriangleList[i][j] = vecTriangles[i][j] - 1;
				m_pTriangleList[i].obj = this;
            }

            return true;
        }
        else
            return false;
    }

    bool Object::CheckParse(int nVertices,std::vector<Triangle> & vecTriangles)
    {
        for(int i=0;i<vecTriangles.size();i++)
        {
            Triangle & vIndices = vecTriangles[i];
            for(int j=0;j<vIndices._len;j++)
                if(vIndices[j]<=0||vIndices[j]>nVertices)
                {
                    printf("Error: The vertex index of Face %d has exceeded vertex number %d\n",i,nVertices);
                    return false;
                }
        }

        return true;
    }

    bool Object::SaveToObj(const char* fn)
    {
        if(!IsLoaded())
        {
            printf("Error: Object is not initialized.\n",fn);
            return false;
        }
        
        FILE* fp = fopen(fn,"w");
        if(fp==NULL)
        {
            printf("Error: Failed opening %s to write\n",fn);
            return false;
        }

        fprintf(fp,"# %d vertices\n",m_nVertices);
        for(int i=0;i<m_nVertices;i++)
            fprintf(fp,"v %f %f %f\n",  m_pVertexList[i].x,
                                        m_pVertexList[i].y,
                                        m_pVertexList[i].z);

        fprintf(fp,"# %d triangles\n",m_nTriangles);
        for(int i=0;i<m_nTriangles;i++)
            fprintf(fp,"f %d %d %d\n",  m_pTriangleList[i][0] + 1,
                                        m_pTriangleList[i][1] + 1,
                                        m_pTriangleList[i][2] + 1);

        fclose(fp);
        
        printf("Writing to %s successfully\n",fn);
        return true;

    }

	Vec3f Object::ObjectIntersect(Ray& r, bool& flag)
	{
		float maxdis = 2e10;
		Vec3f res;
		flag = false;
		Vec3f intersection;
		bool intered = false;
		float dis = 0;
		for (int i = 0; i < m_nTriangles; i++)
		{
			intered = false;
			intersection = r.Intersection(m_pTriangleList[i], intered, dis);
			if (!intered) continue;
			if (dis < maxdis)
			{
				maxdis = dis;
				res = intersection;
				flag = true;
				r.N = m_pTriangleList[i].GetNormal();
			}
		}
		return res;
	}

	void Object::setkvalue(Vec3f& _ka, Vec3f& _ks, Vec3f& _kd)
	{
		ka = _ka;
		ks = _ks;
		kd = _kd;
	}

}


