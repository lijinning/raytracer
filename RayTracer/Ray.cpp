#include "Ray.h"
#include "Vec3f.h"
#include "Object.h"
namespace Tracer
{

	bool Ray::parall(Tracer::Triangle& t)
	{
		if (zero(direction.Dot(t.GetNormal()))) return true;
		return false;
	}

	float Ray::Intersect_At(Triangle& t)
	{
		if (parall(t))
			return -1;
		return abs((t(0) - origin).Dot(t.GetNormal()) / direction.Dot(t.GetNormal()));
	}

	Vec3f Ray::GenerateIntersect(Triangle& tge, bool& flag, float& out_dis)
	{
		float t = Intersect_At(tge);
		if (t == -1)
		{
			flag = false;
			return Vec3f();
		}
		else
		{
			flag = true;
			return getpoint(t);
		}
	}

	bool Ray::PointinTriangle(Triangle& tge, Vec3f P)
	{
		Vec3f v0 = tge(2) - tge(0);
		Vec3f v1 = tge(1) - tge(0);
		Vec3f v2 = P - tge(0);

		float dot00 = v0.Dot(v0);
		float dot01 = v0.Dot(v1);
		float dot02 = v0.Dot(v2);
		float dot11 = v1.Dot(v1);
		float dot12 = v1.Dot(v2);

		float inverDeno = 1 / (dot00 * dot11 - dot01 * dot01);
		float u = (dot11*dot02 - dot01*dot12) * inverDeno;
		if (u < 0 || u > 1)
			return false;
		float v = (dot00 * dot12 - dot01 * dot02) * inverDeno;
		if (v < 0 || v > 1)
		{
			return false;
		}
		return u + v <= 1;
	}

	Vec3f Ray::Intersection(Triangle& tge, bool& flag, float out_dis)
	{
		bool exist = false;
		Vec3f p = GenerateIntersect(tge, exist, out_dis);
		if (!exist || !PointinTriangle(tge, p))
		{
			flag = false;
			return Vec3f();
		}
		flag = true;
		return p;
	}






}