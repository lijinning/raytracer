#pragma once
#include "Vec3f.h"
namespace Tracer
{
	enum LIGHT_TYPE { Undefine, Direction, Point, Spot };
	class LightSource
	{
	public:
		Vec3f pos;
		Vec3f ka;
		Vec3f ks;
		Vec3f kd;
		LIGHT_TYPE LightType;
		float shininess;
	public:
		LightSource() { LightType = Undefine;}
		void LightSouce_Set(const Vec3f& _pos, Vec3f _ka, Vec3f _ks, Vec3f _kd, int _type);
		Vec3f EvalAmbient(const Vec3f& _material_ka);
		Vec3f EvalDiffuse(Vec3f& _N, Vec3f& _L, const Vec3f& _material_kd);
		Vec3f EvalSpecluar(Vec3f& _N, Vec3f& _L, Vec3f& _V, Vec3f& material_ks, float shininess);
		void setvariable(Vec3f& _ka, Vec3f& _ks, Vec3f& _kd, int type, float shini, Vec3f& _pos);
	};






}