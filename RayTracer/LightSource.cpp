#include "LightSource.h"
#include "Vec3f.h"
#include "math.h"

namespace Tracer
{

	void LightSource::LightSouce_Set(const Vec3f& _pos, Vec3f _ka, Vec3f _ks, Vec3f _kd, int _type)
	{
		pos = _pos; ka = _ka;
		ks = _ks; kd = _kd;
		LightType = (LIGHT_TYPE)_type;
	}
	
	Vec3f LightSource::EvalAmbient(const Vec3f& _material_ka)
	{
		return ka * _material_ka;
	}

	Vec3f LightSource::EvalDiffuse(Vec3f& _N, Vec3f& _L, const Vec3f& _material_kd)
	{
		Vec3f KD_EX = kd * _material_kd;
		float NL = _N.Dot(_L);
		if (NL <= 0) NL = 0;
		return KD_EX * NL;
	}

	Vec3f LightSource::EvalSpecluar(Vec3f& _N, Vec3f& _L, Vec3f& _V, Vec3f& material_ks, float shininess)
	{
		Vec3f KS_EX = ks * material_ks;
		Vec3f H = (_L + _V).Normalize();
		float NL = _N.Dot(_L);
		if (NL <= 0) NL = 0;
		float NH = _N.Dot(H);
		if (NH <= 0) NH = 0;
		NH = pow(NH, shininess);
		if (NL <= 0)
			NH = 0;
		return KS_EX * NH;
	}

	void LightSource::setvariable(Vec3f& _ka, Vec3f& _ks, Vec3f& _kd, int type, float shini, Vec3f& _pos)
	{
		ka = _ka;
		ks = _ks;
		kd = _kd;
		LightType = (LIGHT_TYPE)type;
		shininess = shini;
		pos = _pos;
	}
}