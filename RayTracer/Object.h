#pragma once
#include "Vec3f.h"
#include <vector>

namespace Tracer
{
	bool zero(float f);

	class Triangle;
	class Object;
	class Ray;

	class Triangle 
    {
    public:
		Object* obj;
        enum {_len = 3};
    public:
		Triangle() {};
		Triangle(Object* _obj) { obj = _obj; }
		int& operator[] (int i);
		const int& operator[] (int i) const;
		Vec3f operator() (int i);
		const Vec3f operator() (int i) const;
    protected:
        int _p[3];
	public:
		Vec3f GetNormal();
    };


    class Object
    {
		friend Triangle;
    public:
        Object(void);
        ~Object(void);
        
    public:
        bool IsLoaded() { return m_pVertexList!=NULL;}
        void Destroy();
        bool LoadFromObj(const char* fn);
        bool SaveToObj(const char* fn);
        
    protected:
        bool Parse(FILE* fp);
        bool CheckParse(int nVertices,std::vector<Triangle> & vecTriangles);

	public:

        int             m_nVertices;
        int             m_nTriangles;
        Vec3f*          m_pVertexList;
        Triangle*		m_pTriangleList;
		Vec3f			ka;
		Vec3f			ks;
		Vec3f			kd;
		Vec3f			MaxPoint;
		Vec3f			MinPoint;
	public:
		Vec3f ObjectIntersect(Ray& r, bool& flag);
		void setkvalue(Vec3f& ka, Vec3f& ks, Vec3f& kd);
    };

}
