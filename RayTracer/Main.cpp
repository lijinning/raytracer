#include <cstdio>
#include <iostream>
#include <cstring>
#include <cmath>
#include <string>
#include <cstdlib>
#include <stdio.h>
#include <algorithm>
#include <assert.h>
#include "Object.h"
#include "Vec3f.h"
#define STBI_ONLY_PNG
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"
#include "screen.h"
#include "Vec3f.h"
#include "Ray.h"
#include "LightSource.h"

using namespace Tracer;
Vec3f spotpos;
Vec3f screen_origin;
Vec3f screen_dir1;
Vec3f screen_dir2;
int w;
int h;
int comp = 3;
Vec3f index[4];
int ltype = 0;
float in_shininess;

int main()
{	
	freopen("test.txt", "r", stdin);
	Tracer::Object obj;
	obj.LoadFromObj("cube.obj");
	printf("Please set position of spotpos:\n");
	scanf("%f%f%f", &spotpos.x, &spotpos.y, &spotpos.z);
	printf("please set w & h:\n");
	scanf("%d%d%d", &w, &h, &comp);
	STBI_SCREEN scr(w, h, comp);
	printf("please set variable of screen:\n");
	scanf("%f%f%f", &screen_origin.x, &screen_origin.y, &screen_origin.z);
	scanf("%f%f%f", &screen_dir1.x, &screen_dir1.y, &screen_dir1.z);
	scanf("%f%f%f", &screen_dir2.x, &screen_dir2.y, &screen_dir2.z);
	scr.STBI_SCREEN_SET_POS(screen_origin, screen_dir1, screen_dir2);
	printf("Please set material variable ka ks kd:\n");
	scanf("%f%f%f", &index[0].x, &index[0].y, &index[0].z);
	scanf("%f%f%f", &index[1].x, &index[1].y, &index[1].z);
	scanf("%f%f%f", &index[2].x, &index[2].y, &index[2].z);
	obj.setkvalue(index[0], index[1], index[2]);
	printf("Please set LightSource variable ka ks kd:\n");
	scanf("%f%f%f", &index[0].x, &index[0].y, &index[0].z);
	scanf("%f%f%f", &index[1].x, &index[1].y, &index[1].z);
	scanf("%f%f%f", &index[2].x, &index[2].y, &index[2].z);
	printf("Please set LightSouce Shininess & pos:\n");
	scanf("%f%f%f%f", &in_shininess, &index[3].x, &index[3].y, &index[3].z);
	scanf("%d", &ltype);
	LightSource LS;
	LS.setvariable(index[0], index[1], index[2], ltype, in_shininess, index[3]);
	scr.STBI_SCREEN_SET_PTR(&spotpos, &obj, &LS);
	scr.STBI_SCREEN_CAL_COLOR();
	unsigned char* image;
	image = new unsigned char[w * h * comp];
	scr.STBI_SCREEN_SHOW(1);
	scr.STBI_SCREEN_OUTPUT(image);
	stbi_write_png("testout.png", w, h, comp, image, 0);
	return 0;
}