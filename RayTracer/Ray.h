#pragma once
#include "Vec3f.h"

namespace Tracer
{
	bool zero(float f);
	class Object;
	class Triangle;
	class Ray
	{
		friend Object;
	public:
		Vec3f origin;
		Vec3f direction;
		Vec3f N;
	public:
		Ray() {}
		Ray(const Ray& other) {
			origin = other.origin;
			direction = other.direction;
		}
		Ray(const Vec3f& ori, const Vec3f& dir) { origin = ori, direction = dir; }
		void set_origin(const Vec3f& other) { origin = other; }
		void setdirection(const Vec3f& other) { direction = other; }
		Vec3f getorigin() { return origin; }
		Vec3f getdirection() { return direction; }
		Vec3f getpoint(float t) { return origin + direction * t; }
		bool parall(Tracer::Triangle& t);
		float Intersect_At(Triangle& t);
		Vec3f GenerateIntersect(Triangle& tge, bool& flag, float& out_dis);
		bool PointinTriangle(Triangle& tge, Vec3f P);
		Vec3f Intersection(Triangle& tge, bool& flag, float out_dis);
	};


}